package com.sigmagroup.pycca.ui.supervisor.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sigmagroup.pycca.R;
import com.sigmagroup.pycca.databinding.ItemRequestBinding;
import com.sigmagroup.pycca.model.Request;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.ViewHolder> {
    private List<Request> requestsList;
    private ItemClickListener mItemClickListener;

    public RequestAdapter() {
        requestsList = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ItemRequestBinding binding = ItemRequestBinding.inflate(layoutInflater, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindsTo(requestsList.get(position));
    }

    @Override
    public int getItemCount() {
        return requestsList.size();
    }

    public void setRequestsList(List<Request> requestsList) {
        this.requestsList = requestsList;
        notifyDataSetChanged();
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    public void removeItem(int position) {
        requestsList.remove(position);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ItemRequestBinding mBinding;

        public ViewHolder(ItemRequestBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        private void bindsTo(Request request) {
            String by = String.format(itemView.getContext().getString(R.string.requested_by),
                    request.getBy());
            String type = String.format(itemView.getContext().getString(R.string.request_type),
                    request.getType());
            mBinding.requestedBy.setText(by);
            mBinding.requestType.setText(type);
            itemView.setOnClickListener(view -> mItemClickListener.onItemClick(request, getAdapterPosition()));
        }
    }

    public interface ItemClickListener {
        void onItemClick(Request request, int position);
    }
}
