package com.sigmagroup.pycca.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;

import com.microsoft.identity.client.AuthenticationCallback;
import com.microsoft.identity.client.IAuthenticationResult;
import com.microsoft.identity.client.IPublicClientApplication;
import com.microsoft.identity.client.ISingleAccountPublicClientApplication;
import com.microsoft.identity.client.PublicClientApplication;
import com.microsoft.identity.client.exception.MsalException;
import com.sigmagroup.pycca.ui.supervisor.activity.MainActivity;
import com.sigmagroup.pycca.R;
import com.sigmagroup.pycca.databinding.ActivityLoginBinding;
import com.sigmagroup.pycca.utils.Preferences;

public class LoginActivity extends AppCompatActivity {

    private ActivityLoginBinding mBinding;
    private Preferences preferences;
    private ISingleAccountPublicClientApplication mSingleAccountApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        preferences = new Preferences(this);
        PublicClientApplication.createSingleAccountPublicClientApplication(this,
                R.raw.auth_config_single_account,
                new IPublicClientApplication.ISingleAccountApplicationCreatedListener(){
                    @Override
                    public void onCreated(ISingleAccountPublicClientApplication application) {
                        mSingleAccountApp = application;
                        Log.i("LOGIN", "Initialize");
                    }

                    @Override
                    public void onError(MsalException exception) {
                        exception.printStackTrace();
                        Log.e("LOGIN", exception.getMessage());
                    }
                });

        mBinding.btnLogin.setOnClickListener(view -> emailLogin());
    }

    private void emailLogin() {
        String user = mBinding.userInput.getText().toString().trim();
        String password = mBinding.passwordInput.getText().toString().trim();
        mBinding.userInputLayout.setError(null);
        mBinding.passwordInputLayout.setError(null);
        boolean isOK = true;

        if (user.isEmpty()) {
            mBinding.userInputLayout.setError("Escriba su usuario");
            isOK = false;
        } else if(!Patterns.EMAIL_ADDRESS.matcher(user).matches()) {
            mBinding.userInputLayout.setError("Tu dirección de correo no está bien escrita");
            isOK = false;
        }
        if (password.isEmpty()) {
            mBinding.passwordInputLayout.setError("Escriba su contraseña");
            isOK = false;
        }

        if (isOK) {
            preferences.setBoolean(Preferences.sessionStarted, true);
            preferences.setString(Preferences.userEmail, user);
            mainActivity();
        }
    }

    private void mainActivity() {
        Intent mainScreenIntent = new Intent(LoginActivity.this, MainActivity.class);
        mainScreenIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mainScreenIntent);
        finish();
    }

    public void microsoftLogin(View view) {
        if (mSingleAccountApp == null) {
            return;
        }

        mSingleAccountApp.signIn(this, null, new String[]{"user.read"}, getAuthInteractiveCallback());
    }

    private AuthenticationCallback getAuthInteractiveCallback() {
        return new AuthenticationCallback() {

            @Override
            public void onSuccess(IAuthenticationResult authenticationResult) {
                /* Successfully got a token, use it to call a protected resource - MSGraph */
                Log.d("LOGIN", "Successfully authenticated");
                Log.d("LOGIN", "User: " + authenticationResult.getAccount().getUsername());
                preferences.setString(Preferences.userEmail, authenticationResult.getAccount().getUsername());
                mainActivity();
            }

            @Override
            public void onError(MsalException exception) {
                /* Failed to acquireToken */
                Log.d("LOGIN", "Authentication failed: " + exception.toString());
                Log.e("LOGIN", exception.getMessage());
            }

            @Override
            public void onCancel() {
                /* User canceled the authentication */
                Log.d("LOGIN", "User cancelled login.");
            }
        };
    }
}