package com.sigmagroup.pycca.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {
    public static final String sessionStarted = "SESSION_STARTED";
    public static final String supervisorID = "SUPERVISOR_ID";
    public static final String userEmail = "USER_EMAIL";

    private final SharedPreferences preferences;
    private final SharedPreferences.Editor editor;

    public Preferences(Context context) {
        String PREF_NAME = "pyccaConfig";
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public boolean getBoolean(String name) {
        return preferences.getBoolean(name, false);
    }

    public void setBoolean(String name, boolean value) {
        editor.putBoolean(name, value);
        editor.apply();
    }

    public String getString(String name) {
        return preferences.getString(name, "");
    }

    public void setString(String name, String value) {
        editor.putString(name, value);
        editor.apply();
    }
}
