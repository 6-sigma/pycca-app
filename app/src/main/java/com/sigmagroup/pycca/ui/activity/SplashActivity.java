package com.sigmagroup.pycca.ui.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.microsoft.identity.client.IAccount;
import com.microsoft.identity.client.IPublicClientApplication;
import com.microsoft.identity.client.ISingleAccountPublicClientApplication;
import com.microsoft.identity.client.PublicClientApplication;
import com.microsoft.identity.client.exception.MsalException;
import com.sigmagroup.pycca.R;
import com.sigmagroup.pycca.ui.supervisor.activity.MainActivity;
import com.sigmagroup.pycca.utils.Preferences;

public class SplashActivity extends AppCompatActivity {
    private final String TAG = SplashActivity.class.getSimpleName();

    private ISingleAccountPublicClientApplication mSingleAccountApp;
    private IAccount mAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Preferences preferences = new Preferences(this);
        if(preferences.getBoolean(Preferences.sessionStarted)) {
            mainActivityIntent();
        } else {
            PublicClientApplication.createSingleAccountPublicClientApplication(this,
                    R.raw.auth_config_single_account,
                    new IPublicClientApplication.ISingleAccountApplicationCreatedListener() {
                        @Override
                        public void onCreated(ISingleAccountPublicClientApplication application) {
                            mSingleAccountApp = application;
                            loadAccount();
                        }

                        @Override
                        public void onError(MsalException exception) {
                            Log.e(TAG, exception.getMessage());
                            loginActivityIntent();
                        }
                    });
        }
    }

    private void loadAccount() {
        if (mSingleAccountApp == null) {
            loginActivityIntent();
            return;
        }

        mSingleAccountApp.getCurrentAccountAsync(new ISingleAccountPublicClientApplication.CurrentAccountCallback() {
            @Override
            public void onAccountLoaded(@Nullable IAccount activeAccount) {
                // You can use the account data to update your UI or your app database.
                mAccount = activeAccount;
                if (mAccount == null) {
                    loginActivityIntent();
                    return;
                }
                Log.i("LOGIN", mAccount.getUsername());
                Log.i("LOGIN", mAccount.getIdToken());
                mainActivityIntent();
            }

            @Override
            public void onAccountChanged(@Nullable IAccount priorAccount, @Nullable IAccount currentAccount) {
            }

            @Override
            public void onError(@NonNull MsalException exception) {
                Log.e("LOGIN", exception.getMessage());
                loginActivityIntent();
            }
        });
    }

    private void mainActivityIntent() {
        Intent mainScreenIntent = new Intent(this, MainActivity.class);
        startActivity(mainScreenIntent);
        finish();
    }

    private void loginActivityIntent() {
        Intent loginIntent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(loginIntent);
        finish();
    }
}