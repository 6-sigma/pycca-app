package com.sigmagroup.pycca;

import android.app.Application;

import androidx.appcompat.app.AppCompatDelegate;

public class PyccaApplication extends Application {

    @Override
    public void onCreate() {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate();
    }
}
