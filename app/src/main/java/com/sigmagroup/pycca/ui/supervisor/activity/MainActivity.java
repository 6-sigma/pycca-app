package com.sigmagroup.pycca.ui.supervisor.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.microsoft.identity.client.IPublicClientApplication;
import com.microsoft.identity.client.ISingleAccountPublicClientApplication;
import com.microsoft.identity.client.PublicClientApplication;
import com.microsoft.identity.client.exception.MsalException;
import com.sigmagroup.pycca.R;
import com.sigmagroup.pycca.databinding.ActivityMainBinding;
import com.sigmagroup.pycca.databinding.DialogRequestBinding;
import com.sigmagroup.pycca.model.Request;
import com.sigmagroup.pycca.ui.activity.LoginActivity;
import com.sigmagroup.pycca.ui.supervisor.adapter.RequestAdapter;
import com.sigmagroup.pycca.utils.Preferences;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private final String TAG = MainActivity.class.getSimpleName();

    private ActivityMainBinding mBinding;
    private RequestAdapter mRequestAdapter;
    private Preferences mPreferences;

    private ISingleAccountPublicClientApplication mSingleAccountApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        PublicClientApplication.createSingleAccountPublicClientApplication(this,
                R.raw.auth_config_single_account,
                new IPublicClientApplication.ISingleAccountApplicationCreatedListener() {
                    @Override
                    public void onCreated(ISingleAccountPublicClientApplication application) {
                        mSingleAccountApp = application;
                    }

                    @Override
                    public void onError(MsalException exception) {
                        Log.e(TAG, exception.getMessage());
                    }
                });

        mPreferences = new Preferences(this);
        setSupportActionBar(mBinding.appBar.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mRequestAdapter = new RequestAdapter();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mBinding.rcvRequests.setLayoutManager(layoutManager);
        mBinding.rcvRequests.setAdapter(mRequestAdapter);

        mBinding.refreshLayout.setOnRefreshListener(this::dummyData);

        mRequestAdapter.setItemClickListener((request, position) ->
            createRequestDetailDialog(request, position).show());

        dummyData();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_bar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.logout) {
            logoutUser();
        }
        return super.onOptionsItemSelected(item);
    }

    private void dummyData() {
        List<Request> requests = new ArrayList<>();
        Request discountRequest = new Request("Caja 1", "Aplicación de descuento", "Se solicita un descuento del 40% para varios productos");
        discountRequest.setDiscount(40.00);
        requests.add(discountRequest);
        requests.add(new Request("Caja 2", "Nota de crédito", "Nota de crédito no. 00001555555"));
        requests.add(new Request("Caja 3", "Devoluciones", "Cliente quiere devolver productos que ya salieron del local"));
        mRequestAdapter.setRequestsList(requests);
        mBinding.refreshLayout.setRefreshing(false);
        mBinding.rcvRequests.setVisibility(View.VISIBLE);
        mBinding.emptyList.setVisibility(View.GONE);
    }

    private AlertDialog createRequestDetailDialog(Request request, int position) {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
        DialogRequestBinding dialogBinding = DialogRequestBinding.inflate(getLayoutInflater());
        dialogBinding.dialogTitle.setText(request.getType());
        dialogBinding.dialogMessage.setText(request.getDescription());
        builder.setView(dialogBinding.getRoot());
        AlertDialog dialog = builder.create();
        dialogBinding.btnAccept.setOnClickListener(view -> {
            String comments = dialogBinding.commentsInput.getText().toString();
            dialogBinding.commentsInputLayout.setError(null);
            if(comments.trim().isEmpty()) {
                dialogBinding.commentsInputLayout.setError("Escriba sus comentarios");
                return;
            }
            removeItemFromAdapter(position);
            dialog.dismiss();
        });
        dialogBinding.btnReject.setOnClickListener(view -> {
            String comments = dialogBinding.commentsInput.getText().toString();
            dialogBinding.commentsInputLayout.setError(null);
            if(comments.trim().isEmpty()) {
                dialogBinding.commentsInputLayout.setError("Escriba sus comentarios");
                return;
            }
            removeItemFromAdapter(position);
            dialog.dismiss();
        });

        return dialog;
    }

    private void removeItemFromAdapter(int position) {
        mRequestAdapter.removeItem(position);
        if (mRequestAdapter.getItemCount() == 0) {
            mBinding.rcvRequests.setVisibility(View.GONE);
            mBinding.emptyList.setVisibility(View.VISIBLE);
        }
    }

    private void logoutUser() {
        if (mPreferences.getBoolean(Preferences.sessionStarted))
            logoutEmailUser();
        else
            logoutMicrosoftUser();
    }

    private void logoutEmailUser() {
        mPreferences.setBoolean(Preferences.sessionStarted, false);
        returnToLogin();
    }

    private void logoutMicrosoftUser() {
        if (mSingleAccountApp == null) {
            return;
        }

        mSingleAccountApp.signOut(new ISingleAccountPublicClientApplication.SignOutCallback() {
            @Override
            public void onSignOut() {
                returnToLogin();
            }

            @Override
            public void onError(@NonNull MsalException exception) {
                Log.e(TAG, exception.getMessage());
            }
        });
    }

    private void returnToLogin() {
        mPreferences.setString(Preferences.userEmail, null);
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(loginIntent);
        finish();
    }
}