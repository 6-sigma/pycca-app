package com.sigmagroup.pycca.model;

public class Request {
    private String by;
    private String type;
    private String description;
    private double discount;

    public Request() {}

    public Request(String by, String type, String description) {
        this.by = by;
        this.type = type;
        this.description = description;
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }
}
